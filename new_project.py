import pyautogui
import time

def videofile_project():
    pyautogui.click(704, 625, duration=0.15)
    pyautogui.time.sleep(5)
    # video file
    pyautogui.click(689, 585)
    # season
    pyautogui.typewrite('test')
    # Competition
    pyautogui.click(192, 163, duration=0.15)
    pyautogui.typewrite('test 1')
    # Description
    pyautogui.click(130, 208, duration=0.15)
    pyautogui.typewrite('test 3')
    
def select_video():
    # Select video
    pyautogui.moveTo(1216, 205, duration=0.25)
    pyautogui.click(1217, 206, duration=0.15)
    # Video path
    pyautogui.typewrite("C:/Users/Jessica/Desktop/videos/Rifteam20feb2018.webm")
    pyautogui.press('enter')
    pyautogui.time.sleep(2)

def add_angle():
    # Add angle 1
    pyautogui.click(1213, 245, duration=0.15)
    # select video
    pyautogui.typewrite("C:/Users/Jessica/Desktop/videos/Rifteam20feb2018.webm")
    pyautogui.press('enter')

def ok():
    pyautogui.moveTo(1883, 79, duration=0.25)
    pyautogui.click(1884, 78)