import open_app
import new_project

def main ():
    open_app.open_app()
    # Maximize windows
    open_app.max_windows()
    # Create video file project
    new_project.videofile_project()
    new_project.select_video()
    # Add second angle
    new_project.add_angle()
    new_project.ok()

main()
